# JYeontu 组件仓库

#### 介绍

日常开发组件/工具库
vue 组件 + js 函数工具

封装一些有趣（实用的小组件），后续会不断迭代优化。

#### 安装教程

##### 本地启动项目

1. git clone https://gitee.com/zheng_yongtao/jyeontu-component-warehouse.git
2. npm install
3. npm run serve

##### 项目引入

###### npm 安装

推荐使用 npm 的方式安装，它能更好地和 webpack 打包工具配合使用。

```shell
npm i @jyeontu/jvuewheel -S
```

###### main.js 引入

引入后便可以在项目中直接使用

```shell
//引入组件库
import jvuewheel from '@jyeontu/jvuewheel'
//引入样式
import '@jyeontu/jvuewheel/lib/jvuewhell.css'
Vue.use(jvuewheel);
```

#### 效果预览

[预览地址(使用文档)](http://120.79.163.94/JYeontuComponents/#/homePage)

#### 相关说明

组件文档：[http://120.79.163.94/JYeontuComponents/#/homePage](http://120.79.163.94/JYeontuComponents/#/homePage)

组件实现说明：[https://juejin.cn/column/7326801745261330482](https://juejin.cn/column/7326801745261330482)

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

#### 联系

CSDN：[https://blog.csdn.net/Twinkle_sone](https://blog.csdn.net/Twinkle_sone)

Gitee：[https://gitee.com/zheng_yongtao](https://gitee.com/zheng_yongtao)

GitHub：[https://github.com/yongtaozheng](https://github.com/yongtaozheng)

掘金：[https://juejin.cn/user/440244290727294](https://juejin.cn/user/440244290727294)

公众号：前端也能这么有趣

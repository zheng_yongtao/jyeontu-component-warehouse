export function sendUrlMsg(api, interval = 5 * 60 * 1000) {
    const url = location.href.split("#")[0];
    const key = url + "burialPoint";
    const lastTime = localStorage.getItem(key) || 0;
    const nowDateTime = new Date().getTime();
    if (nowDateTime - lastTime < interval) return;
    const params = {
        url,
    };
    fetch(api, {
        method: "POST",
        headers: {
            "Content-type": "application/json",
        },
        body: JSON.stringify(params),
    }).then(() => {
        localStorage.setItem(key, nowDateTime);
    });
}

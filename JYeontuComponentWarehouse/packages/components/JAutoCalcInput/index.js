import JAutoCalcInput from "./src/JAutoCalcInput.vue";

JAutoCalcInput.install = (Vue) =>
    Vue.component(JAutoCalcInput.name, JAutoCalcInput); //注册组件

export default JAutoCalcInput;

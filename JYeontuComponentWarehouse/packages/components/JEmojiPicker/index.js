import JEmojiPicker from "./src/JEmojiPicker.vue";

JEmojiPicker.install = Vue =>
    Vue.component(JEmojiPicker.name, JEmojiPicker); //注册组件

export default JEmojiPicker;

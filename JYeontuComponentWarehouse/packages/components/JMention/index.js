import JMention from "./src/JMention.vue";

JMention.install = (Vue) => Vue.component(JMention.name, JMention); //注册组件

export default JMention;

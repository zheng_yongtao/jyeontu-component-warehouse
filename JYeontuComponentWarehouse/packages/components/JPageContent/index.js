import JPageContent from "./src/JPageContent.vue";

JPageContent.install = (Vue) => Vue.component(JPageContent.name, JPageContent); //注册组件

export default JPageContent;

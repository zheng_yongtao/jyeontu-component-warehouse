import JVideoAdvertisement from "./src/JVideoAdvertisement.vue";

JVideoAdvertisement.install = (Vue) =>
    Vue.component(JVideoAdvertisement.name, JVideoAdvertisement); //注册组件

export default JVideoAdvertisement;

import JVideoSpeed from "./src/JVideoSpeed.vue";

JVideoSpeed.install = (Vue) => Vue.component(JVideoSpeed.name, JVideoSpeed); //注册组件

export default JVideoSpeed;

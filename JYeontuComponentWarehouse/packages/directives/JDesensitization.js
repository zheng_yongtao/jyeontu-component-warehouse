let fouceListerner = null,
    blurListerner = null;
const desensitization = (str, config, fn) => {
    str = str.trim() + "";
    if (fn) {
        return fn(str);
    }
    if (!str) {
        return "";
    }
    let { before = 0, after = 0 } = config;
    const { type, ch = "*" } = config;
    const len = str.length;
    switch (type) {
        case "phone":
            before = "3";
            after = "4";
            if (len < 11) {
                before = 2;
                after = 2;
            }
            break;
        case "email":
            before = "1";
            if (str.split("@").length > 1) after = str.split("@")[1].length + 1;
            break;
        case "idCard":
            before = "3";
            after = "4";
            break;
        case "name":
            before = "1";
            after = len > 2 ? 1 : 0;
            break;
    }
    const show = Math.max(len - before - after, 0);
    const reg = [
        new RegExp(`^(.{${before}}).{${show}}(.{${after}})$`),
        `$1${ch.repeat(show)}$2`,
    ];
    return str.replace(reg[0], reg[1]);
};
function doDesensitization(el, binding) {
    const params = binding.value || {};
    const { config, fn } = params;
    let inputOriginVal = "";
    let originVal = el.getAttribute("desensitization-originVal") || "";
    if (el.innerHTML) {
        if (!originVal) {
            el.setAttribute("desensitization-originVal", el.innerText);
            originVal = el.innerText;
        }
        el.innerHTML = desensitization(originVal, config, fn);
        return;
    }
    if (fouceListerner) {
        el.removeEventListener("focus", fouceListerner);
    }
    fouceListerner = el.addEventListener("focus", (event) => {
        event.target.value = inputOriginVal;
    });
    if (blurListerner) {
        el.removeEventListener("blur", blurListerner);
    }
    blurListerner = el.addEventListener("blur", (event) => {
        const value = event.target.value;
        inputOriginVal = value;
        const newVal = desensitization(value, config, fn);
        event.target.value = newVal;
    });
}
export default {
    bind: function (el, binding) {
        try {
            doDesensitization(el, binding);
        } catch (err) {
            console.error("bindErr", err);
        }
    },
    update: function (el, binding) {
        if (!el.innerText) return;
        try {
            doDesensitization(el, binding);
        } catch (err) {
            console.error("updateErr", err);
        }
    },
    unbind: function (el) {
        if (fouceListerner) {
            el.removeEventListener("focus", fouceListerner);
        }
        if (blurListerner) {
            el.removeEventListener("blur", blurListerner);
        }
    },
};

function CreateDropdown(el, binding) {
    const dropdown = document.createElement("ul");
    dropdown.className = "mention-dropdown";
    Object.assign(dropdown.style, {
        display: "none",
        position: "absolute",
        border: "1px solid #ddd",
        maxHeight: "200px",
        overflowY: "auto",
        paddingInlineStart: 0,
        width: el.offsetWidth + "px",
        backgroundColor: "#fff",
        zIndex: 9999,
    });
    return dropdown;
}

function renderDropdown(users, dropdown, el) {
    const seleced = el._mentions;
    dropdown.innerHTML = users
        .filter((user) => !seleced.has(user.id + ""))
        .map(
            (user) => `
      <li data-username="${user.name}" data-userid="${user.id}" class="mention-item-li"
          style="padding:1em;cursor:pointer;list-style-type: none;" >
        ${user.name}
      </li>
    `
        )
        .join("");
    dropdown.style.display = "block";
}

function addHoverStyle(className) {
    const style = document.createElement("style");
    style.textContent = `
    .${className}:hover {
      background-color: #f1f1f1;
    }
  `;
    document.head.appendChild(style);
}

function addClassStyle(className, styleStr) {
    const style = document.createElement("style");
    style.textContent = `
    .${className} {${styleStr}}
    `;
    document.head.appendChild(style);
}

function positionDropdown(el, dropdown) {
    const rect = el.getBoundingClientRect();
    dropdown.style.top = `${rect.bottom + window.scrollY}px`;
    dropdown.style.left = `${rect.left + window.scrollX}px`;
}

// 移动光标到指定位置
function moveCaretToIndex(el, targetIndex) {
    const selection = window.getSelection();
    if (selection.rangeCount <= 0) return;
    const range = document.createRange();
    let currentIndex = 0;
    let node;
    const walk = document.createTreeWalker(el, NodeFilter.SHOW_TEXT, null);
    while ((node = walk.nextNode())) {
        const nodeLength = node.textContent.length;
        if (currentIndex + nodeLength >= targetIndex) {
            const offset = Math.min(targetIndex - currentIndex, nodeLength);
            range.setStart(node, offset);
            range.setEnd(node, offset);
            break;
        }
        currentIndex += nodeLength;
    }
    selection.removeAllRanges();
    selection.addRange(range);
}

export default {
    inserted: function (el, binding, vnode) {
        el._mentions = new Set(); // 存储被@人员
        const dropdown = CreateDropdown(el, binding);
        el._dropdown = dropdown;
        el.parentNode.appendChild(dropdown);
        const { list } = binding.value;
        addHoverStyle("mention-item-li");
        el.setAttribute("contenteditable", true);
        addClassStyle(
            "mention-item-span",
            "background-color: #f1f1f1; user-select: text; -webkit-user-select: text; -moz-user-select: text; -ms-user-select: text;"
        );

        // 输入监听
        el.addEventListener("input", function handler(e) {
            const selection = window.getSelection();
            if (selection.rangeCount <= 0) return;
            const range = selection.getRangeAt(0);
            const startOffset = range.startOffset;
            const startContainer = range.startContainer;
            let textBeforeCursor = "";
            if (startContainer.nodeType === Node.TEXT_NODE) {
                textBeforeCursor = startContainer.textContent.slice(
                    0,
                    startOffset
                );
            } else if (startContainer.nodeType === Node.ELEMENT_NODE) {
                // 若光标在元素节点内，获取其内部文本
                const childNodes = startContainer.childNodes;
                for (let i = 0; i < childNodes.length; i++) {
                    const node = childNodes[i];
                    if (node.nodeType === Node.TEXT_NODE) {
                        if (i === childNodes.length - 1) {
                            textBeforeCursor += node.textContent.slice(
                                0,
                                startOffset
                            );
                        } else {
                            textBeforeCursor += node.textContent;
                        }
                    }
                }
            }
            const lastChar = textBeforeCursor.slice(-1);
            if (lastChar === "@") {
                renderDropdown(list, dropdown, el);
                positionDropdown(el, dropdown);
            } else {
                dropdown.style.display = "none";
            }
        });

        // 点击选择
        dropdown.addEventListener("click", (e) => {
            if (e.target.tagName === "LI") {
                const username = e.target.dataset.username;
                const userid = e.target.dataset.userid;
                const selection = window.getSelection();
                if (selection.rangeCount <= 0) return;
                const range = selection.getRangeAt(0);
                const preCaretRange = range.cloneRange();
                preCaretRange.selectNodeContents(el);
                preCaretRange.setEnd(range.endContainer, range.endOffset);
                const index = preCaretRange.toString().length;

                // 定位并移除 @ 字符
                let currentIndex = 0;
                let atNode = null,
                    node = null;
                let atOffset = 0;
                const walk = document.createTreeWalker(
                    el,
                    NodeFilter.SHOW_TEXT,
                    null
                );
                while ((node = walk.nextNode())) {
                    const nodeLength = node.textContent.length;
                    if (currentIndex + nodeLength >= index - 1) {
                        atNode = node;
                        atOffset = index - 1 - currentIndex;
                        break;
                    }
                    currentIndex += nodeLength;
                }
                if (atNode && atNode.textContent[atOffset] === "@") {
                    // atNode.deleteData(atOffset, 1);
                    atNode.replaceData(atOffset, 1, " ");
                }

                // 创建包含 @ 和用户名的 span 元素
                const span = document.createElement("span");
                span.contentEditable = false;
                span.className = "mention-item-span";
                span.textContent = `@${username}`;
                span.setAttribute("data-username", username);
                span.setAttribute("data-userid", userid);

                const tmp = document.createElement("span");
                tmp.contentEditable = true;
                tmp.appendChild(span);
                tmp.innerHTML += "&nbsp;"; // 为了避免 span 被合并，添加空格

                // 在当前光标位置插入 span 元素
                // range.insertNode(span);
                range.insertNode(tmp);

                // 计算插入 <span> 后新的目标位置
                let newTargetIndex = index;
                newTargetIndex += span.textContent.length;

                moveCaretToIndex(el, newTargetIndex);

                el._mentions.add(userid);
                el.mentionlist = [...el._mentions];
                const mentionListName = el.getAttribute("mentionList");
                vnode.context[mentionListName] = [...el._mentions];
                dropdown.style.display = "none";
                el.focus();
            }
        });
        // 监听 span 被删除事件
        const observer = new MutationObserver((mutationsList) => {
            for (const mutation of mutationsList) {
                if (mutation.type === "childList") {
                    const removedNodes = Array.from(
                        mutation.removedNodes
                    ).filter(
                        (node) =>
                            node.classList &&
                            node.classList.contains("mention-item-span")
                    );
                    if (removedNodes.length > 0) {
                        removedNodes.forEach((removedNode) => {
                            console.log(
                                "%c Line:215 🍤 removedNode",
                                "color:#4fff4B",
                                removedNode
                            );
                            const userid = removedNode.dataset.userid;
                            if (userid) {
                                el._mentions.delete(userid);
                                el.mentionlist = [...el._mentions];
                                const mentionListName =
                                    el.getAttribute("mentionList");
                                vnode.context[mentionListName] = [
                                    ...el._mentions,
                                ];
                            }
                        });
                    }
                }
            }
        });

        observer.observe(el, { childList: true, subtree: true });
    },
    bind: function (el, binding) {
        try {
            // doDesensitization(el, binding);
        } catch (err) {
            console.error("bindErr", err);
        }
    },
    update: function (el, binding) {
        if (!el.innerText) return;
        try {
            // doDesensitization(el, binding);
        } catch (err) {
            console.error("updateErr", err);
        }
    },
    unbind: function (el) {
        el.parentNode.removeChild(el._dropdown);
        delete el._mentions;
    },
};

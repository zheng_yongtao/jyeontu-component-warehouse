const numberToChineseWords = (num, unit = "元", append = "整") => {
    // 汉字的数字
    const cnNums = ["零", "壹", "贰", "叁", "肆", "伍", "陆", "柒", "捌", "玖"];
    // 基本单位
    const cnIntRadice = ["", "拾", "佰", "仟"];
    // 对应整数部分扩展单位
    const cnIntUnits = ["", "万", "亿", "兆"];
    // 对应小数部分单位
    const cnDecUnits = ["角", "分", "毫", "厘"];
    // 最大处理的数字
    const maxNum = 9999999999999999.99;
    // 金额整数部分
    let integerNum;
    // 金额小数部分
    let decimalNum;
    // 输出的中文金额字符串
    let chineseStr = "";
    // 分离金额后用的数组，预定义
    let parts;
    if (num === "") {
        return "";
    }
    num = parseFloat(num);
    if (num >= maxNum) {
        // 超出最大处理数字
        return "";
    }
    if (num == 0) {
        chineseStr = cnNums[0] + unit + append;
        return chineseStr;
    }
    // 转换为字符串
    num = num.toString();
    if (num.indexOf(".") === -1) {
        integerNum = num;
        decimalNum = "";
    } else {
        parts = num.split(".");
        integerNum = parts[0];
        decimalNum = parts[1].substring(0, 4);
    }
    if (parseInt(integerNum, 10) > 0) {
        let zeroCount = 0;
        let IntLen = integerNum.length;
        for (let i = 0; i < IntLen; i++) {
            let n = integerNum[i];
            let p = IntLen - i - 1;
            let q = p / 4;
            let m = p % 4;
            if (n === "0") {
                zeroCount++;
            } else {
                if (zeroCount > 0) {
                    chineseStr += cnNums[0];
                }
                // 归零
                zeroCount = 0;
                chineseStr += cnNums[parseInt(n)] + cnIntRadice[m];
            }
            if (m === 0 && zeroCount < 4) {
                chineseStr += cnIntUnits[q];
            }
        }
        chineseStr += unit;
    }
    // 小数部分
    if (decimalNum !== "") {
        let decLen = decimalNum.length;
        for (let i = 0; i < decLen; i++) {
            let n = decimalNum[i];
            if (n !== "0") {
                chineseStr += cnNums[Number(n)] + cnDecUnits[i];
            }
        }
    }
    if (chineseStr === "") {
        chineseStr += cnNums[0] + unit + append;
    } else if (decimalNum === "") {
        chineseStr += append;
    }
    return chineseStr;
};
export default {
    bind: function (el, binding) {
        const params = binding.value || {};
        const { unit = "", append = "" } = params;
        let originVal = el.innerText;
        if (el.innerHTML) {
            el.innerHTML = numberToChineseWords(el.innerText, unit, append);
        }
        el.addEventListener("focus", (event) => {
            event.target.value = originVal;
        });
        el.addEventListener("blur", (event) => {
            const value = event.target.value;
            originVal = value;
            event.target.value = numberToChineseWords(value, unit, append);
        });
    },
};

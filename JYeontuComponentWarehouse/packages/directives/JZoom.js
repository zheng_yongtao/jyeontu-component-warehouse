export default {
    inserted: function (el) {
        const run = () => {
            //获取随机id
            const getRandom = (min, max) => {
                return Math.floor(Math.random() * (max - min) + min);
            };

            const randomId = getRandom(1000000, 9999999);
            //需要添加指令的元素外层套上一层盒子
            const box = document.createElement("div");
            box.style.position = "relative";
            box.classList = "j-zoom-box";

            el.style.position = "relative";
            const bounding = el.getBoundingClientRect();
            const content = document.createElement("div");
            content.classList = "j-zoom-content";
            const parentElement = el.parentElement;
            parentElement.removeChild(el);
            content.appendChild(el);
            content.style.position = "absolute";
            content.style.height = bounding.height + "px";
            content.style.width = bounding.width + "px";
            box.style.height = bounding.height + "px";
            box.style.width = bounding.width + "px";
            el.style.height = "100%";
            el.style.width = "100%";
            box.appendChild(content);
            parentElement.appendChild(box);

            let isMouseDown = "";
            let defaultPos = {};

            const rectWidth = 20;

            function zoomDomMouseDown(e, flag) {
                isMouseDown = flag;
                const bounding = content.getBoundingClientRect();
                defaultPos = {
                    left: parseFloat(content.style.left || 0),
                    top: parseFloat(content.style.top || 0),
                    width: parseFloat(content.style.width || 0),
                    height: parseFloat(content.style.height || 0),
                    x: bounding.x,
                    y: bounding.y,
                    startX: e.clientX || e.touches[0].clientX,
                    startY: e.clientY || e.touches[0].clientY,
                };
                document.addEventListener("mousemove", zoomDomMouseOver);
                document.addEventListener("mouseup", zoomDomMouseUp);
                document.addEventListener("touchmove", zoomDomMouseOver);
                document.addEventListener("touchend", zoomDomMouseUp);
            }
            function zoomDomMouseOver(event) {
                if (!isMouseDown) return;
                const clientX = event.clientX || event.touches[0].clientX,
                    clientY = event.clientY || event.touches[0].clientY;

                let addWidth = clientX - defaultPos.startX;
                let addHeight = clientY - defaultPos.startY;

                if (isMouseDown.includes("top")) {
                    addHeight = -addHeight;
                }
                if (isMouseDown.includes("Left") || isMouseDown === "left") {
                    addWidth = -addWidth;
                }

                const topMid = content.querySelector("#" + "topMid" + randomId);
                const leftMid = content.querySelector(
                    "#" + "leftMid" + randomId
                );
                const rightMid = content.querySelector(
                    "#" + "rightMid" + randomId
                );
                const bottomMid = content.querySelector(
                    "#" + "bottomMid" + randomId
                );

                if (!["top", "bottom"].includes(isMouseDown)) {
                    content.style.width = defaultPos.width + addWidth + "px";
                    topMid.style.width =
                        defaultPos.width + addWidth - rectWidth * 2 + "px";
                    bottomMid.style.width =
                        defaultPos.width + addWidth - rectWidth * 2 + "px";
                }
                if (!["left", "right"].includes(isMouseDown)) {
                    content.style.height = defaultPos.height + addHeight + "px";
                    leftMid.style.height =
                        defaultPos.height + addHeight - rectWidth * 2 + "px";
                    rightMid.style.height =
                        defaultPos.height + addHeight - rectWidth * 2 + "px";
                }

                if (isMouseDown.includes("top")) {
                    content.style.top =
                        Math.min(
                            defaultPos.top - addHeight,
                            defaultPos.top + defaultPos.height
                        ) + "px";
                    if (
                        defaultPos.top - addHeight >
                        defaultPos.top + defaultPos.height
                    ) {
                        content.style.height = 0 + "px";
                    }
                }
                if (isMouseDown.includes("Left") || isMouseDown === "left") {
                    content.style.left =
                        Math.min(
                            defaultPos.left - addWidth,
                            defaultPos.left + defaultPos.width
                        ) + "px";
                    if (
                        defaultPos.left - addWidth >
                        defaultPos.left + defaultPos.width
                    ) {
                        content.style.width = 0 + "px";
                    }
                }
                if (isMouseDown.includes("bottom")) {
                    content.style.top = defaultPos.top + "px";
                }
                if (isMouseDown.includes("Right")) {
                    content.style.left = defaultPos.left + "px";
                }
            }

            function zoomDomMouseUp() {
                isMouseDown = "";
                document.removeEventListener("mousemove", zoomDomMouseOver);
                document.removeEventListener("mouseup", zoomDomMouseUp);
                document.addEventListener("touchmove", zoomDomMouseOver);
                document.addEventListener("touchend", zoomDomMouseUp);
            }

            function createHornRect(position) {
                const isTop = position.includes("top");
                const rect = document.createElement("div");
                rect.style.width = rectWidth + "px";
                rect.style.height = rectWidth + "px";
                rect.style.position = "absolute";
                rect.style.background = "transparent";
                // rect.style.background = "red";
                if (isTop) rect.style.top = 0;
                else rect.style.bottom = 0;
                rect.style.left = position.includes("Left") ? "0" : "";
                rect.style.right = position.includes("Right") ? "0" : "";
                const map = {
                    topLeft: "nw",
                    topRight: "ne",
                    bottomLeft: "sw",
                    bottomRight: "se",
                };
                rect.style.cursor = map[position] + "-resize";
                return rect;
            }
            function obj2Style(dom, obj) {
                for (const k in obj) {
                    dom.style[k] = obj[k];
                }
            }

            function createAndAddHorn(position) {
                const horn = createHornRect(position);
                content.appendChild(horn);
                horn.addEventListener("mousedown", (e) => {
                    zoomDomMouseDown(e, position);
                });
                horn.addEventListener("touchstart", (e) => {
                    zoomDomMouseDown(e, position);
                });
            }
            function createEdgeElement(position) {
                const edge = document.createElement("div");
                edge.id = position + "Mid" + randomId;
                let height = rectWidth,
                    width = rectWidth;
                if (position === "top" || position === "bottom")
                    width = bounding.width - rectWidth * 2;
                else height = bounding.height - rectWidth * 2;
                const cursorMap = {
                    top: "n-resize",
                    bottom: "s-resize",
                    left: "w-resize",
                    right: "e-resize",
                };
                obj2Style(edge, {
                    [["top", "bottom"].includes(position) ? "left" : "top"]:
                        rectWidth + "px",
                    [position]: 0,
                    width: width + "px",
                    position: "absolute",
                    height: height + "px",
                    cursor: cursorMap[position],
                    background: "transparent",
                    // background: "yellow",
                });
                return edge;
            }
            function createAndAddEdgeElement(position) {
                const edge = createEdgeElement(position);
                content.appendChild(edge);
                edge.addEventListener("mousedown", (e) => {
                    zoomDomMouseDown(e, position);
                });
                edge.addEventListener("touchstart", (e) => {
                    zoomDomMouseDown(e, position);
                });
            }
            function addElement() {
                ["topLeft", "topRight", "bottomLeft", "bottomRight"].forEach(
                    (position) => {
                        createAndAddHorn(position);
                    }
                );
                ["top", "bottom", "left", "right"].forEach((position) => {
                    createAndAddEdgeElement(position);
                });
            }
            addElement();
        };
        run();
    },
};
